#scrapymanager

from random import randint
from time import sleep
from subprocess import call
import multiprocessing
import sys

def go(call_args):
    check, crawl_incomplete = True, False
    crawler_id,posts_completed, pages_completed, restarts, total_restarts, max_posts = None, 0, 0, 0, 0, int(call_args['posts'])
    call_str = str('scrapy crawl stack_crawl_2 -a query='+call_args['query']+' -a pageL='+call_args['pageL']+' -a pageH='+call_args['pageH']+' -a sort='+call_args['sort']+' -a size='+call_args['size']+' -a posts='+call_args['posts']+' -a crawler_id='+call_args['crawler_id']+" -a restarts="+str(restarts))
    call(call_str, shell=True)

    while check:
        with open('checkpoint.txt','r') as checkfile:
            for line in checkfile:
                line = line.strip("\n").split(',')
                crawler_id, pages_completed, restarts = line[0], int(line[1]), int(line[2])
                if line[0] == call_args['crawler_id'] and restarts == total_restarts:
                    posts_completed += (pages_completed * int(call_args['size']))
                    if posts_completed < max_posts:
                        crawl_incomplete = True
                        print("CRAWLER",crawler_id,"INCOMPLETE, INITIATING RESTART",restarts)
                    elif posts_completed >= max_posts:
                        crawl_incomplete = False
                    break

        # If this checkpoint entry's crawler_id matches this crawler call's crawler_id
        if crawl_incomplete:
            restarts += 1
            total_restarts += 1
            # Set lower page bound to last completed page, subtract completed posts from number of remaining posts.
            call_args['pageL'] = str(int(call_args['pageL']) + pages_completed)
            call_args['posts'] = str(int(call_args['posts']) - (pages_completed * int(call_args['size'])))
            call_str = str('scrapy crawl stack_crawl_2 -a query='+call_args['query']+' -a pageL='+call_args['pageL']+' -a pageH='+call_args['pageH']+' -a sort='+call_args['sort']+' -a size='+call_args['size']+' -a posts='+call_args['posts']+' -a crawler_id='+call_args['crawler_id']+' -a restarts='+str(restarts))
            call(call_str, shell=True)
        elif not crawl_incomplete and posts_completed == int(call_args['posts']):
            print("CRAWL COMPLETE")
            check = False
        else:
            print("CRAWLER ERROR")
            exit(1)
    return 0

def initialize(query,pages,sort,size,offset):
    if __name__ == '__main__':
        pages, f = int(pages), open('checkpoint.txt','w')
        f.close()
        pages_per_crawler = []
        count = multiprocessing.cpu_count() - 2
        pool = multiprocessing.Pool(processes=count)
        call_strings = []

        for i in range(0,count):
            pages_per_crawler.append(int(pages/count))
        pages_per_crawler[len(pages_per_crawler)-1] = pages - (pages_per_crawler[0] * (count -1))

        for c_num, pages_crawl, in enumerate(pages_per_crawler):
            start = int(pages/count) * c_num + 1 + offset
            end = start + pages_crawl + endset
            print("START",start,"END",end)
            num_posts = (end - start) * int(size)
            call_str = {'query': query, 'pageL': str(start), 'pageH': str(end), 'sort': sort, 'size': size, 'posts': str(num_posts), 'crawler_id': str(c_num)}
            call_strings.append(call_str)
        call('cd /Users/Seam/Documents/Projects/bsca_stackscraper', shell=True)
        pool.map(go,call_strings)

initialize(sys.argv[1],sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
