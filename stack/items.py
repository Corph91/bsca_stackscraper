# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class StackItem(Item):
    """
        Each Stackoverflow article scraped should have its title, url,
        question asked, and accepted *or* present answers stored. If no
        answers are provided, this should be denoted by an appropriate
        message.
    """
    title = Field()
    url = Field()

    def __repr__(self):
        """Stop Scrapy from puking output to terminal"""
        return ""
