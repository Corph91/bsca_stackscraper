"""
    Stackoverflow Web Crawler
    Sean Corbett - BSCA
    06/02/2017
    v2.0 (v1.0 got rekt)
"""

# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http.request import Request
from stack.items import StackItem
import urllib.parse
import re
import time

class StackCrawl2Spider(scrapy.Spider):
    """
        Main crawler class. Name variable is global to ensure it is reachable
        by other files in program.
    """
    name = "stack_crawl_2"

    def __init__(self, query=None, pageL=None, pageH=None, sort="newest", size="50", posts=None, crawler_id=None, restarts=None, proxy=None, *args, **kwargs):
        """
            Crawler takes four args, number of pages to crawl and query. Number
            of pages should be a valid integer, while query should be a valid
            string (argument should be surrounded by quotes when running
            crawler). Sort and size args are optional, and will default to
            'newest' and 50 results per. page respectively.

            Crawler manually generates list of pages to crawl. I tried using
            the Rules, but they consistently didn't take the pagination links.
        """
        print("I AM CRAWLER",crawler_id,"RESTART",restarts,"START-PAGE",pageL,"END-PAGE",pageH)

        self.crawler_id, self.sizePage, self.restarts, self.num_posts  = crawler_id, int(size), restarts, posts
        self.start_urls, self.count, self.pagesCrawled, self.allowed_domains  = [], 0, 0, ['stackoverflow.com']
        self.proxies, self.proxySet, self.proxy_crawl = [], False, ""

        super(StackCrawl2Spider, self).__init__(*args, **kwargs)
        
        if proxy:
            self.proxy_crawl = proxy
        # General template for Stackoverflow searches sorted by most frequently
        # asked and displaying fifty results per page.
        urlTemplate = "https://stackoverflow.com/questions/tagged/{query}?page={page}&sort={sort}&pagesize={size}"
        query = urllib.parse.quote(query)

        # Generate a list of start urls to follow based on number of pages set
        # in args.
        for x in range(int(pageL),int(pageH)):
            self.start_urls.append(urlTemplate.format(query=query,page=x,sort=sort,size=size))

    def start_requests(self):
        for url in self.start_urls:
            self.current_url = url
            yield Request(self.current_url, self.parse, errback=self.handle_error)

    def handle_error(self,failure):
        print("Error Handle: %s" % failure.request)
        print("Sleeping")
        time.sleep(60)
        yield Request(self.current_url, self.parse, errback=self.handle_error)

    def handle_secondary_error(self,failure):
        print("Error Handle Secondary: %s" % failure.request)
        print("Sleeping")
        time.sleep(60)
        yield Request(self.current_url, self.parse, errback=self.handle_error)

<<<<<<< HEAD
    def closed(self, reason):
        if self.count != self.num_posts:
            print("SCRAPING INCOMPLETE",self.count ,"/",self.num_posts)
            with open('checkpoint.txt','a+') as checkfile:
                checkfile.write(self.crawler_id+','+str(self.pagesCrawled)+","+str(self.restarts)+"\n")

        else:
            print("SCRAPING COMPLETE")
            exit()
=======
>>>>>>> 35ec3b40b3ff2d1ebf5da1f5b5fb6e9971996662

    def parse(self, response):
        """
            Main parse function crawls a list of articles on Stackoverflow,
            returning their title and url, then recursively calls parse_item()
            parser to pull question and answers from article page.
        """
        # Find all questions on this page.
        questions = response.xpath('//div[@class="summary"]/h3')

        # For each question on this page, parse its title and generate a valid
        # url link to that article. Store this link, then recursively call the
        # parse_item function to gather additional information.
        for question in questions:
            item = StackItem()
            self.link = question.xpath('a[@class="question-hyperlink"]/@href').extract()[0]
            item['title'] = question.xpath('a[@class="question-hyperlink"]/text()').extract()[0]

            # Create valid url from href data from article snippet, store and
            # then recursively follow with request using parse_item().
            self.post_url = "https://stackoverflow.com" + ''.join(self.link)
            item['url'] = self.post_url
<<<<<<< HEAD
            new_request = Request(self.post_url, callback=self.parse_item, errback=self.handle_secondary_error)
            new_request.meta['item'] = item
            yield new_request


    def parse_item(self,response):
        """
            Page crawler function searches Stackoverflow article for the
            question asked and answers. Note that the crawler will first look
            for an accepted answer (peer verified).

            If no such answer is found, the crawler will then search for all
            other answers provided and store these answers in a list while
            setting the accepted_answer field to 'None' (to denote that no
            validated answer was found).

            If no answers are found for a question, the crawler sets the
            'answers' field to 'No answers for question found'.
        """

        # Pass the current Stackoverflow article item being processed and find
        # question (then store it).
        item = response.meta['item']
        question = response.xpath('//*[@id="question"]')

        # Extract and cleanup data
        question = question.xpath('.//div[@class="post-text"]//text()').extract()
        question = re.sub(r"\s+", " ", ' '.join(question).rstrip('\r\n'))
        item['question'] = question

        accepted_answer=response.xpath('//*[contains(@class,"accepted-answer")]')

        #If we find an accepted answer, use only this answer for the question.
        if accepted_answer:
            # Extract and cleanup data.
            accepted_answer = response.xpath('.//div[@class="post-text"]//text()').extract()
            accepted_answer = re.sub(r"\s+", " ", ' '.join(accepted_answer).rstrip('\r\n')).replace(item['question'],"")
            item['accepted_answer'] = accepted_answer
        # Else we should search for all answers to the question and store them.
        else:
            item['accepted_answer'] = 'None'
            answers = response.xpath('//*[contains(@class,"answer")]')
            if answers:
                found_answers = []
                for answer in answers:
                    # Extract and cleanup data
                    answer = response.xpath('.//div[@class="post-text"]//text()').extract()
                    answer = re.sub(r"\s+", " ", ' '.join(answer).rstrip('\r\n')).replace(item['question'],"")
                    found_answers.append(answer)
                item['answers'] = found_answers
            else:
                item['answers'] = "No answers found to question."

        self.count += 1

        if self.count > 0 and self.count%self.sizePage == 0:
            self.pagesCrawled +=1
        return item
=======
            self.pagesCrawled += 1
            yield item
>>>>>>> 35ec3b40b3ff2d1ebf5da1f5b5fb6e9971996662
