# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo

from scrapy.conf import settings
from scrapy.exceptions import DropItem
from scrapy import log


class MongoDBPipeline(object):

    def __init__(self):
        """
            Initialize connection with MongoDB, then create database
            "Stackoverflow" and collection "questions" if they do not
            already exist. Otherwise reopen the database and collection for
            insertion of new data.
        """
        connection = pymongo.MongoClient(settings['MONGODB_SERVER'],settings['MONGODB_PORT'])
        db = connection[settings['MONGODB_DB']]
        self.collection = db[settings['MONGODB_COLLECTION']]
        self.added = 0

    def process_item(self, item, spider):
        """
            Verify that for incoming Stackoverflow item no fields are missing.
            If data is valid, insert item into collection if it is not already
            there, otherwise update existing record.
        """
        for data in item:
            if not data:
                raise DropItem("Missing data!")
        self.collection.update({'url': item['url']}, dict(item), upsert=True)

        # Keep track of how many articles have completely finished processing
        # and storage.
        self.added += 1
        print("Finished:",self.added,"records.\n")
        return item
