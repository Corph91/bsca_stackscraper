clea# Big Sky Code Academy #
# Stack Scraper v2.0 #

This is the initial, working version of a Scrapy web-crawler designed to scrape
Stackoverflow articles (questions). The crawler initially scrapes a
user-determined number of pages for a given (user provided) query, then
recursively follows the links to those articles to further parse the question
asked as well as that question's accepted answer. Lacking an accepted answer,
the crawler will then find and parse all provided answers to the question.

When an article has been fully mined for relevant data, it is then stored in
the programs "stackoverflow" database in MongoDB under the "questions"
collection. These settings can be modified in the:

    settings.py

file.

### REQUIREMENTS: ###
 * Include:
      * -Python 3.x (we used version 3.5.2)
      * -MongoDB (latest version)
      * -Scrapy (and all dependencies)
      * -PyMongo (and all dependencies)


### RUNNING THE PROGRAM: ###
To run the crawler use the following command:

    python spidergen.py <"query"> <num-pages> <num-spiders> <sort-by> <page-count>

Note that all arguments are required. The pages argument
should be a valid integer, and the query should be a valid string surrounded by
quotes. The "sort-by" argument dictates the sorting method used by the
Stackoverflow search pages. Valid options include:

    * -Newest
    * -Frequent
    * -Votes
    * -Active

Likewise, size dictates the number of results per Stackoverflow search page. We
recommend fifty results per page to keep search page requests to a minimum.

### OTHER NOTES: ###
Delay per spider is now controlled by python sleep() function and is set to
randomly alternate between 2-5 seconds per spider. You may modify this range in:

    spidergen.py

to suit your needs, *but remain respectful of Stackoverflow's servers and high
traffic*. Database connection settings beyond the name of the database and the
collection in which results are stored *should not* be modified. Note that
the program also now uses rotating proxies to account for multiple spiders. To
this effect, we recommend setting the number of spiders generated equal to the
number of threads available to your processor minus two.
